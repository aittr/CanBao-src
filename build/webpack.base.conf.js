var path = require('path')
var config = require('../config')
var utils = require('./utils')
var pxtorem = require('postcss-pxtorem');
var autoprefixer = require('autoprefixer');
var projectRoot = path.resolve(__dirname, '../')
var env = process.env.NODE_ENV;
// var ExtractTextPlugin = require('extract-text-webpack-plugin');
// var NyanProgressPlugin = require('nyan-progress-webpack-plugin');

var pageEntries = utils.getEntries('./src/views/**/*.html', 'js');
utils.log('Page Entries', pageEntries);


module.exports = {
  entry: pageEntries,
  output: {
    path: config.build.assetsRoot,
    publicPath: env === 'production' ? config.build.assetsPublicPath : config.dev.assetsPublicPath,
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.web.js', '.js', '.jsx', '.json'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      'src': path.resolve(__dirname, '../src'),
      'Utils': path.resolve(__dirname, '../src/utils'),
      'assets': path.resolve(__dirname, '../src/assets'),
      'components': path.resolve(__dirname, '../src/components')
    }
  },
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  postcss:[
    autoprefixer({browsers:['last 2 versions']}),
    pxtorem({
      rootValue: 100,
      propWhiteList: [],
    })
 ],
  module: {
    // preLoaders: [
    //   {
    //     test: /\.js$/,
    //     loader: 'eslint',
    //     include: projectRoot,
    //     exclude: /node_modules/
    //   }
    // ],
    loaders: [
      {
        test: /\.js[x]?$/,
        include: projectRoot,
        exclude: /node_modules/,
        loader: 'babel',
      }, {
        test: /\.json$/,
        loader: 'json'
      }, {
          test: /\.css$/,
          exclude: /^node_modules$/,
          loaders: ['style', 'css','postcss']
      }, {
          test: /\.less/,
          exclude: /^node_modules$/,
          loaders: ['style', 'css', 'postcss', 'less']
      }, {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      }, {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
        }
      },

    ]
  },
  // eslint: {
  //   formatter: require('eslint-friendly-formatter')
  // },
  plugins: [
    // new NyanProgressPlugin() // 进度条
  ]
}

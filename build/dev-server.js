var path = require('path')
var express = require('express')
var webpack = require('webpack')
var bodyParser = require('body-parser')
var config = require('../config')
var opn = require('opn')
var proxyMiddleware = require('http-proxy-middleware')
var webpackConfig = process.env.NODE_ENV === 'testing'
  ? require('./webpack.prod.conf')
  : require('./webpack.dev.conf')

var env = process.env.NODE_ENV;

// default port where dev server listens for incoming traffic
var PORT = process.env.PORT || config.dev.port
// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
var proxyTable = config.dev.proxyTable

var app = express()
var router = express.Router();
var compiler = webpack(webpackConfig)

var devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: webpackConfig.output.publicPath,
  stats: {
    colors: true,
    chunks: false
  }
})

var hotMiddleware = require('webpack-hot-middleware')(compiler)
// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', function (compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
    hotMiddleware.publish({ action: 'reload' })
    cb()
  })
})

// proxy api requests
// 调试模式下接口请求代理到调试服务器
// if(env === 'testing'){
  Object.keys(proxyTable).forEach(function (context) {
    var options = proxyTable[context]
    if (typeof options === 'string') {
      options = { target: options }
    }
    app.use(proxyMiddleware(context, options))
  })
// }

// 处理请求参数
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use( '/', router );

// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())

// serve webpack bundle output
app.use(devMiddleware)

// enable hot-reload and state-preserving
// compilation error display
app.use(hotMiddleware)

// serve pure static assets
var staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory)
app.use(staticPath, express.static('./static'))
app.use('/lib', express.static('./dist/lib'))

// 本地开发时可以路由到其它单页
app.get('/:viewname.html', function(req, res, next) {
  var viewname = req.params.viewname
    ? req.params.viewname + '.html'
    : 'index.html';
  var filepath = path.join(compiler.outputPath, viewname);

  // 使用webpack提供的outputFileSystem
  compiler.outputFileSystem.readFile(filepath, function(err, result) {
    if (err) {
      // something error
      return next(err);
    }
    res.set('content-type', 'text/html');
    res.send(result);
    res.end();
  });
});

// 开发模式下 使用Server端的模拟数据
// if( env === 'development' ){
//   require('../mock')( router );
// }

//404
app.use(function(req, res, next) {
  res.send('404');
  res.end();
});

//500
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  console.log(err);
  res.send('服务器内部错误');
  res.end();
});

module.exports = app.listen(PORT, function (err) {
  if (err) {
    console.log(err)
    return
  }
  var uri = 'http://localhost:' + PORT;
  console.log('NODE_ENV = '+env);
  console.log('Listening at ' + uri + '\n');
  env === 'development' && opn(uri)
})

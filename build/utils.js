var path = require('path')
var config = require('../config')

var glob = require('glob');

// 获取所有入口页JS
exports.getEntries = function (globPath, type) {
  var entries = {}
  glob.sync(globPath).forEach(function (entry) {
    var basename = path.basename(entry, path.extname(entry))
    entries[basename] = type ? entry.replace(basename+'.html','index.js') : entry;
  });
  return entries;
}

exports.assetsPath = function (_path) {
  var assetsSubDirectory = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectory
    : config.dev.assetsSubDirectory
  return path.posix.join(assetsSubDirectory, _path)
}

exports.log = function (t, c) {
  console.log('---------- '+t+' ----------')
  console.log(c)

  console.log('----------'+'-'.repeat(t.length)+'--------------')
}

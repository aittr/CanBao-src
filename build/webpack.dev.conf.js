var config = require('../config')
var webpack = require('webpack')
var merge = require('webpack-merge')
var utils = require('./utils')
var path = require('path')
var baseWebpackConfig = require('./webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var HtmlInjectPlugin = require('html-inject-webpack-plugin/src')

// add hot-reload related code to entry chunks
// 使用所有的SPA页面都可能热重载
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

module.exports = merge(baseWebpackConfig, {
  // eval-source-map is faster for development
  devtool: '#eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ]
})

// 将webpack打包的js等资源自动注入到页面
// https://github.com/ampedandwired/html-webpack-plugin
var pages = utils.getEntries('./src/views/**/*.html')
for(var page in pages) {
  var conf = {
    filename: page + '.html',
    template: pages[page],
    inject: true,
    excludeChunks: Object.keys(pages).filter(item => {
      //只对入口页做注入
      return (item != page)
    })
  }
  module.exports.plugins.push(new HtmlWebpackPlugin(conf))
}

// 注入公共片段到页面
// https://github.com/maiwenan/html-inject-webpack-plugin
// module.exports.plugins.push(
//   new HtmlInjectPlugin({
//     header: ['./src/partials/header.tpl'],
//     // bodys: [{flagname: 'tabbar', template: path.resolve(__dirname, './tpl/tabbar.html') }],
//     // footer:['']
//   })
// )

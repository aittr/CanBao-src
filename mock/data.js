const Mock  = require('mockjs')

module.exports = {
  // 菜单
  'POST/api/register':()=>{
    return Mock.mock({status: 0, message: 'OK', result:[{key: '1', name: '商户数据', icon: 'user', path: '/data', child: [{name: '子菜单', path: '/data/1', key: '1-1'} ] },{key: '2', name: '商户列表', icon: 'laptop', path: '/list'},{key: '3', name: '商户设置', icon: 'notification', path: '/setup'} ] })
  }
}

const reload = require('require-reload')(require);


module.exports = function( router ){
  let mockData = reload('./data');
  setInterval(function(){
    mockData = reload('./data');
  }, 1000)

  router.all('/api/*', (req, res, next)=>{
    let apiUrl = req.method+req.path;

    console.log('=>api: '+apiUrl);

    let data = mockData[apiUrl] && mockData[apiUrl]() || {status:404, message:'接口不存在'};

    setTimeout(()=>{
      res.json(data)
    }, data.delay || 0)
  })
};

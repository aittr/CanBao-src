/**
 * 秒的倒计时
 * second: 秒数
 * countFn: 计数回调，参数为当前second
 * endFn: 计数完成后的回调
 */
export default class SecondCountDown{
  constructor(second, countFn, endFn){
    this.timer = null;
    this.second = second || 60;
    this.countFn = countFn || Funciton();
    this.endFn = endFn || Funciton();
    this.start();
  }

  start(){
    if(this.second === 0){
      clearTimeout(this.timer)
      this.endFn(this.second)
      return;
    }
    this.countFn(this.second--);
    this.timer = setTimeout(this.start.bind(this), 1000)
  }
}

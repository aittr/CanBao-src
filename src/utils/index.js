import SecondCountdown from './second-countdown'

export default {
  isPromise(value){
    if (value !== null && typeof value === 'object') {
      return value.promise && typeof value.promise.then === 'function';
    }
  },
  getCookie(name){
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  },
  getUrlParam: function (name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },
  merge(target){
    for (let i = 1, j = arguments.length; i < j; i++) {
      let source = arguments[i] || {};
      for (let prop in source) {
        if (source.hasOwnProperty(prop)) {
          let value = source[prop];
          if (value !== undefined) {
            target[prop] = value;
          }
        }
      }
    }
    return target;
  },
  secondCountDown(second, countFn, endFn){
    return new SecondCountdown(second, countFn, endFn);
  },
  isWeixin(){
      var ua = navigator.userAgent.toLowerCase();
      return ua.match(/MicroMessenger/i)=='micromessenger';
  }
}

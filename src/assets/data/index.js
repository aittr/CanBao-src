import district from './district'
import province from './province'

export default {
  district,
  province
}

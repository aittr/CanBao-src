import React,{ Component } from 'react'
import Button from 'components/button/button'

export default class ListPlaceholder extends Component{

  static propTypes={
    icon:React.PropTypes.any,
    text:React.PropTypes.string,
  }

  static defaultProps={
    icon: null,
    text: '暂无数据',
  }

  render(){
    const { icon, text} = this.props;
    return (
      <div data-flex="main:center cross:center" className="list-placeholer-box">
        <div className="pct40 tc">
          {icon ? icon : <i className="icon-list-placeholer icon200"></i> }
          <div className="c-8 f30 mt20">{text}</div>
          <div className="mt30">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

import React, { Component } from 'react';
import 'flex.css/dist/data-flex.css';
import './gift-detail.less'
import Button from 'components/button/button'
import giftImg from 'assets/img/home/4.png'

export default class GiftPage extends Component {
  render () {
    return (
      <div className="product-detail-wrap pt20 page-scroll-view">
        <div className="pro-detail-info mb20 bg-wt">
          <img src={giftImg} alt="" className="pro-detail-img"/>
          <div className="tit" data-flex="main:justify">
            <div className="f32">正面左半区广告位（大）</div>
            <div><span className="f32">900</span> <span className="24">积分/期 <span className="del">1000</span></span> </div>
          </div>
        </div>

        <div className="view-box gift-detail-point">
            <div className="f32 bdr-b pl30 pr30" data-flex="main:justify">
              <div>当前积分：<span>3000</span></div>
              <div>已兑换36个</div>
            </div>
        </div>

        <div className="view-box gift-to-address pl30 pr30">
          <div data-flex="main:justify">
            <p className="f32">配送至</p>
            <p className="c-8 f28">您还未选择配送地址</p>
          </div>
        </div>

        <div className="pro-detail-desc view-box">
          <div className="inner pl30">
            <div className="tit bdr-b f30 pt20 pb20">产品描述：</div>
            <div className="con f28 c-8 pt20 pb20">
              本广告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告
            </div>
          </div>
        </div>

        <div className="btn-fixed-bottom bg-wt bdr-t">
          <Button>立即兑换</Button>
        </div>
      </div>
    )
  }
}

// <div className=""><Button size="small">第四期</Button></div>

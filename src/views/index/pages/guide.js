import React from 'react'
import { Link } from 'react-router'
import QueueAnim from 'rc-queue-anim';
import './guide.less'

export default class GuidePage extends React.Component {
  constructor () {
    super()
  }
  render () {
    return (
      <div className="guid-page-wrap">
        <div className="page-list-header pb30">
          <h2 className="f30 mb20">可访问的页面列表</h2>
          <p className="c-8 f24">以下页面可以预先预览，数据交互会逐渐完善</p>
        </div>
        <QueueAnim delay={350} className="queue-simple">
          <div className="page-list-item" key="1" style={{background:'#4ECDC4'}}>
            <Link to="/?type=1">首页</Link>
          </div>
          <div className="page-list-item" key="2" style={{background:'#FF6B6B'}}>
            <Link to="register?type=1">注册页</Link>
          </div>
          <div className="page-list-item" key="3" style={{background:'#4E6D8A'}}>
            <Link to="invitation">介绍分发商</Link>
          </div>
          <div className="page-list-item" key="4" style={{background:'#556270'}}>
            <Link to="account">账户安全</Link>
          </div>
          <div className="page-list-item" key="5" style={{background:'#7E93A6'}}>
            <Link to="editPhone">修改手机号</Link>
          </div>
          <div className="page-list-item" key="6" style={{background:'#F7A19B'}}>
            <Link to="editEmail">修改邮箱</Link>
          </div>
          <div className="page-list-item" key="7" style={{background:'#7E4A80'}}>
            <Link to="authen">认证</Link>
          </div>
          <div className="page-list-item" key="8" style={{background:'#FEA083'}}>
            <Link to="pickAddress">选择地址</Link>
          </div>
          <div className="page-list-item" key="9" style={{background:'#FF6C00'}}>
            <Link to="points">积分明细</Link>
          </div>
          <div className="page-list-item" key="10" style={{background:'#EF067B'}}>
            <Link to="addAddress">添加地址</Link>
          </div>
          <div className="page-list-item" key="11" style={{background:'#483C6C'}}>
            <Link to="recharge">提现</Link>
          </div>
          <div className="page-list-item" key="12" style={{background:'#34CFBE'}}>
            <Link to="orderList">我的分发</Link>
          </div>
        </QueueAnim>
      </div>
    )
  }
}

// editEmail
// authen
// pickAddress
// points

// #EF067B
// #D90F7A
// #34CFBE
//
// #483C6C
// #483C6C
// #009E8E
// #FFA500
// <div className="page-list-item" key="4" style={{background:'#FF6B6B'}}>
//   <Link to="register">注册页</Link>
// </div>
// <div className="page-list-item" key="5" style={{background:'#556270'}}>
//   <Link to="register">注册页</Link>
// </div>

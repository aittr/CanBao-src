import React, { Component } from 'react';
import 'flex.css/dist/data-flex.css';
import './authen.less'
import Immutable from 'seamless-immutable'
import Uploader from 'components/form/uploader'
import Button from 'components/button/button'
import { connect } from 'react-redux'
import { Toast } from 'antd-mobile'
import ListPlaceholder from '../modules/m-list-placeholder'
// import { uploadImgs } from '../../../store/actions'
const iSlider = require('islider.js')
require('islider.js/build/iSlider.animate.min.js')
require('islider.js/build/iSlider.plugin.dot.min.js')

let sliderInstance;

@connect(
  state => state.app
)
export default class AuthenPage extends React.Component {
  constructor(){
    super();
  }
  componentDidMount(){
    // sliderInstance = new iSlider(document.getElementById('authenSliderWrap'), [] );
    let { medias=[] } = this.props.userInfo;
    const hasImgs = !!medias.length;
    if(!hasImgs) return;

    let data = medias.map(v=>{return { content: v.img }});
    const wrapperObj = document.getElementById('authenSliderWrap');
    data = JSON.parse(JSON.stringify(data))

    sliderInstance = new iSlider({
      dom: wrapperObj,
      data:data,
      fixPage:false,
      isLooping: 1,
      isOverspread: 1,
      isAutoplay: false,
      animateTime: 800,
      animateType: 'flow',
      plugins: ['dot']
    });

    wrapperObj.addEventListener('click', (e)=>{
      if(e.target.tagName === 'IMG'){
        wx.previewImage({
            current: e.target.src,
            urls: data.map(v=>v.content)
        });
      }
    })

  }
  componentWillUnmount(){
    sliderInstance && sliderInstance.destroy();
  }

  render () {
    let { medias=[] } = this.props.userInfo;
    const hasImgs = !!medias.length;

    return (
      <div ref="formBox" className="authen-wrapper page-scroll-view bg-wt">
        {
          hasImgs
            ? <div id="authenSliderWrap"></div>
            : <ListPlaceholder text="暂无证件照片"></ListPlaceholder>
        }
      </div>
    )
  }
}


//
//
// constructor(){
//   super();
//   this.state={
//     ims:[],
//     wxUploadRes:[]
//   }
//   this.uploadImgSucccess = this.uploadImgSucccess.bind(this);
//   this.uploadImgDelete = this.uploadImgDelete.bind(this);
//   this.submitImgsEv = this.submitImgsEv.bind(this);
// }
// uploadImgSucccess(res){
//   this.setState({
//     wxUploadRes: this.state.wxUploadRes.concat(res)
//   })
// }
// uploadImgDelete(imgId){
//   let resData = this.state.wxUploadRes;
//   resData = resData.filter(v=>v.localId!==imgId);
//   this.setState({ wxUploadRes: resData });
// }
// submitImgsEv(a, e){
//   // 如果是认证中和认证成功的就提示不能修改
//   if(this.props.userInfo.res===0){
//     return Toast.info('正在认证中，请等待');
//   }
//   if(this.props.userInfo.res===1){
//     return Toast.info('请毋重复认证');
//   }
//   this.props.dispatch(uploadImgs({imgs: this.state.wxUploadRes.map(v=>v.serverId).join(',')}))
// }

// <div className="f30">
//   说明：请将企业/商家营业执照全照拍摄上传，要求照片清晰，不得遮挡。
// </div>
// <div className="authen-upload-box">
//   <Uploader title={'上传营业执照'}
//     onSuccessed={ this.uploadImgSucccess }
//     onDelete = { this.uploadImgDelete }
//     files={ this.state.wxUploadRes.map(v=>v.localId)}></Uploader>
// </div>
// <div className="btn-fixed-bottom">
//   <Button onClick={this.submitImgsEv} disabled={this.state.wxUploadRes.length===0}>确定</Button>
// </div>

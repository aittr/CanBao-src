import React, { Component } from 'react';
import 'flex.css/dist/data-flex.css';
import './points.less'
import ListPlaceholder from '../modules/m-list-placeholder'
import { connect } from 'react-redux'
import { getPoints } from '../../../store/actions'
import moment from 'moment'

moment.locale('zh-cn');

@connect(
  state=>state.point
)
export default class PointsPage extends Component {
  componentDidMount(){
    this.props.dispatch(getPoints({page:0, size:20}));
  }
  getTimeText(t){
    const tNow = moment(moment(new Date()).format('YYYY-MM-DD 00:00:00'));
    if(moment(t).isAfter(tNow)) return '今天';
    if(moment(t).isAfter(tNow.subtract(1, 'days'))) return '昨天';
    return moment(t).format('MM-DD')
  }
  createItems(list){
      return list.map((v, i)=>{
        return (
          <div className="points-list-item bdr-b" data-flex="box:first" key={i}>
            <div className="left-time" data-flex="cross:center" key={v.id}>
              <div className="c-8 pl20">
                <div className="f26 mb20">{ this.getTimeText(v.tradTs) }</div>
                <div className="f20">{moment(v.tradTs).format('hh:mm')}</div>
              </div>
            </div>
            <div className="right-point" data-flex="cross:center">
              <div>
                <div className="f32 b mb15 c-rd">{v.point>=0 ? `+${v.point}`:`-${v.point}`}积分</div>
                <div className="f28">{v.des}</div>
              </div>
            </div>
          </div>
        )
      })
  }
  render () {
    const { list=[] } = this.props;
    console.log(list)
    return (
      <div className={`points-list-wrap pt20 page-scroll-view ${list.length>0 ? 'notEmpty':''}`}>
          {
            !list.length
            ? <ListPlaceholder></ListPlaceholder>
            : this.createItems(list)
          }
      </div>
    )
  }
}

//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">今天</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15 c-rd">+3000积分</div>
//       <div className="f28">充值300元</div>
//     </div>
//   </div>
// </div>
//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
//
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>
// <div className="points-list-item bdr-b" data-flex="box:first">
//   <div className="left-time" data-flex="cross:center">
//     <div className="c-8 pl20">
//       <div className="f30 mb20">12-23</div>
//       <div className="f20">14:03</div>
//     </div>
//   </div>
//   <div className="right-point" data-flex="cross:center">
//     <div>
//       <div className="f32 b mb15">+3000积分</div>
//       <div className="f28">兑换 "正面软文" 第三期、第四期、第五期</div>
//     </div>
//   </div>
// </div>

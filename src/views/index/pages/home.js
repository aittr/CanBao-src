import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'flex.css/dist/data-flex.css'
import Toptip from 'components/toptip/toptip'
import { validValue } from 'Utils/lite-validator.js'
import { getType }  from 'Utils/auth'
import { Link } from 'react-router'
import Button from 'components/button/button'
import ImgProduct from 'assets/img/home/6.png'
import ImgAvatar5 from 'assets/img/home/avatar5.png'
import './home.less'
import 'assets/css/icon/ui-icons.less'
import { Toast } from 'antd-mobile'
import moment from 'moment'
import { hashHistory } from 'react-router'
import { getSpecialProduct, requestGetUser, getNotice } from '../../../store/actions'

const defUserInfo = {
  address: "杭州市上城区网尚空间0",
  company: "杭州预见未来餐饮管理有限公司",
  id: 201,
  name:'张三',
  point: 0,
  status: '正在验证中',
}

@connect(
  state => state.app
)
export default class HomePage extends Component {
  constructor () {
    super()
    this.chkStatus = this.chkStatus.bind(this)
  }
  componentDidMount(){
    // 获取单个特价商品
    // this.props.dispatch(getSpecialProduct());
    // 刷新用户信息
    this.props.dispatch(requestGetUser({type: getType()}));
    // 获取地心小编信息
    this.props.dispatch(getNotice())
  }
  chkStatus(e){
    const status = this.props.userInfo.res;
    if(status === 0){
      e.preventDefault();
      return Toast.info('正在认证中，请等待');
    }

    // if(status === 2){
    //   e.preventDefault();
    //   Toast.info('认证失败，请重新认证');
    //   hashHistory.replace({
    //     pathname: '/register',
    //     query:{
    //       type: this.props.location.query.type || 1
    //     }
    //   })
    // }
  }
  wait(e){
    e.preventDefault();
    Toast.info('敬请期待...');
    return false;
  }
  createSpecialProduct(pro){
    return (
      <div className="home-spec-notic bg-wt shadow mb20">
        <div className="tit bdr-b" data-flex="dir:left box:justify">
          <div className="tit-left tc">
            <span className="dib bg-center avatar60 bg-avatar1"></span>
          </div>
          <div className="tit-center">
            <div className="pb10 f24">{ pro.name }<span className="c-8 f20"> (剩余2天12小时30分)</span></div>
          </div>
          <div className="tit-right c-8 f24 pt30 tc">
            <Link to={`productDetail?prodId=${pro.id}&isSpecial=1`}>
              <div className="dib tit-right-arrow bg-center icon-arrow-right"></div>
            </Link>
          </div>
        </div>
        <div className="con ovh">
          <Link to={`productDetail?prodId=${pro.id}&isSpecial=1`}>
            <img src={ pro.skuMedia.primary.url } className="img-home-product"/>
          </Link>
          <div className="pro-info">
            <h3 className="f30 pb10">{ pro.name }</h3>
            <p className="f28 c-rd">{ pro.attributeValue }-发行时间：{ moment(pro.activeStartDate).format('YYYY-MM-DD') } - { moment(pro.activeEndDate).format('YYYY-MM-DD') }</p>
          </div>
        </div>
      </div>
    )
  }
  render () {
    let { userInfo={}, userType, notice="" } = this.props;
    !userInfo.company && (userInfo = defUserInfo);

    console.log(notice)

    return (
      <div className="page-home pb75">
        <div className="ui-nav main-nav tc bg-rd" data-flex="box:mean">
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/withdraw" onClick={this.chkStatus} activeClassName="active">
              <i className="icon70 icon-recharge"></i>
              <div className="f30 c-wt mt30">提现</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/points" onClick={this.chkStatus} activeClassName="active">
              <i className="icon70 icon-bill"></i>
              <div className="f30 c-wt mt30">账单</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/orderList" onClick={this.chkStatus} activeClassName="active">
              <i className="icon70 icon-adver"></i>
              <div className="f30 c-wt mt30">分发动态</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/register" onClick={this.wait} activeClassName="active">
              <i className="icon70 icon-map"></i>
              <div className="f30 c-wt mt30">分发地图</div>
            </Link>
          </div>
        </div>
        <div className="ui-nav sub-nav tc bdr-b bg-wt"  data-flex="box:mean">
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/register" onClick={this.wait}>
              <i className="icon50 icon-service"></i>
              <div className="f30 c-8 mt15">小二</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/pickAddress" onClick={this.chkStatus}>
              <i className="icon50 icon-address"></i>
              <div className="f30 c-8 mt15">收货地址</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/account" onClick={this.chkStatus}>
              <i className="icon50 icon-safe"></i>
              <div className="f30 c-8 mt15">账号安全</div>
            </Link>
          </div>

          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/invitation" onClick={this.chkStatus}>
              <i className="icon50 icon-intro"></i>
              <div className="f30 c-8 mt15">推荐分发商</div>
            </Link>
          </div>
        </div>
        <div className="ui-nav sub-nav tc c-8 bg-wt mb20"  data-flex="box:mean">
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/register" onClick={this.wait}>
              <i className="icon50 icon-monitor"></i>
              <div className="f30 c-8 mt15">分发监控</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/authen" onClick={this.chkStatus}>
              <i className="icon50 icon-authen"></i>
              <div className="f30 c-8 mt15">认证</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/register" onClick={this.wait}>
              <i className="icon50 icon-power"></i>
              <div className="f30 c-8 mt15">道可力量</div>
            </Link>
          </div>
          <div className="ui-nav-item" data-flex="main:center cross:center">
            <Link to="/" onClick={this.wait}>
              <i className="icon50 icon-wait"></i>
              <div className="f30 c-8 mt15">loading</div>
            </Link>
          </div>
        </div>
        <div className="home-user-info bg-wt shadow mb20">
          <div className="points c-rd f20 tr dn">
            <div>剩余积分</div>
            <div>{userInfo.point}</div>
          </div>
          <Button size="small" className="btnauth">{userInfo.status}</Button>
          <div className="avatar80 bg-center bg-avatar6" style={{backgroundImage:`url(${userInfo.wximg})`}}></div>
          <div className="avator-points bg-center bg-avatar-points"  data-flex="main:center cross:center">
            <div className="c-rd tc">
              <div className="f28">赚取积分</div>
              <div className="f34">{userInfo.point}</div>
            </div>
          </div>

          <div className="address tc">
            <h2 className="f24 mb15">{userInfo.company}</h2>
            <p className="f20 c-8"><i className="dib bg-center icon-location"></i><a href={`http://uri.amap.com/search?keyword=${userInfo.address}`}>{userInfo.address}</a></p>
          </div>
        </div>

        <div className="home-xiao-bian bg-wt shadow mb20">
          <div className="tit" data-flex="dir:left box:justify">
              <div className="tit-left tc">
                <span className="dib bg-center avatar60 bg-avatar5" style={{backgroundImage:`url(${ImgAvatar5})`}}></span>
              </div>
              <div className="tit-center">
                <div className="f26">地心小编</div>
                <div className="c-8 f20">{moment(new Date()).format('hh:mm')}</div>
              </div>
              <div className="tit-right c-8 f24 pt30">详细状态<i className="bg-center icon-arrow-right mr20"></i></div>
          </div>
          <div className="con tc">
            {
              !notice.companyName
                ? <h4 className="f30 mb10">暂无分发任务，趁机多休息会儿~</h4>
                : <span><h4 className="f30 mb10">{notice.companyName}</h4><div className="c-rd f30">{notice.status}</div></span>
            }
          </div>
        </div>

      </div>
    )
  }
}

  // {specialProduct && (specialProduct.name ?  this.createSpecialProduct(specialProduct) : null) }



// <div className="home-spec-notic bg-wt shadow mb20">
//   <div className="tit bdr-b" data-flex="dir:left box:justify">
//     <div className="tit-left tc">
//       <span className="dib bg-center avatar60 bg-avatar1"></span>
//     </div>
//     <div className="tit-center">
//       <div className="pb10 f24">广告位特价提醒<span className="c-8 f20"> (剩余2天12小时30分)</span></div>
//     </div>
//     <div className="tit-right c-8 f24 pt30 tc">
//       <div className="dib tit-right-arrow bg-center icon-arrow-right"></div>
//     </div>
//   </div>
//   <div className="con ovh">
//     <img src={ImgProduct} alt="" className="img-home-product"/>
//     <div className="pro-info">
//       <h3 className="f30 pb10">反面左半区1/2版面硬广兑换</h3>
//       <p className="f28 c-rd">第三期-发行时间：2016.11.23~2016.12.14.22</p>
//     </div>
//   </div>
// </div>

import React, { Component } from 'react'
import 'flex.css/dist/data-flex.css'
import './mall.less'
import ListPlaceholder from '../modules/m-list-placeholder'
import ImgProduct from 'assets/img/home/6.png'
import giftImg from 'assets/img/home/1.png'
import { Link } from 'react-router'

import { Tabs } from 'antd-mobile'
const TabPane = Tabs.TabPane;

import moment from 'moment'
moment.locale('zh-cn')

import { connect } from 'react-redux'
import { getProducts } from '../../../store/actions'

@connect(
  state=>state.product
)
export default class MallPage extends Component {
  constructor(){
    super();
  }
  callback(key) {
    console.log(key);
  }
  componentDidMount(){
    this.props.dispatch(getProducts({page:0, size:20}))
  }
  render () {
    const { list=[] } = this.props;
    console.log(list);
    const proItems = list.map((v,i)=>{
      return (
        <div className="mall-pro-item mb20 shadow bdr-t bdr-b bg-wt" key={v.id}>
          <Link to={`productDetail?prodId=${v.id}`}>
            <img src={v.skuMedia.primary.url+'?imageslim&imageView2/1/w/750/h/482'} alt="" className="img-home-product"/>
            <div className="pro-info pt30 pb30 ml30">
              <h2 className="f30 pb10">{v.name}</h2>
              <p className="f28 c-rd">发行时间：{moment(v.activeStartDate).format('L')}</p>
            </div>
          </Link>
        </div>
      )
    })

    return (
      <div className="mall-wrap page-scroll-view pb75">
         <div>
           <ListPlaceholder text="暂无礼品"></ListPlaceholder>
         </div>
      </div>
    )
  }
}

// <TabPane tab="广告位" key="1" className="pt20">
//   <div>
//   {
//     list.length
//     ? proItems
//     : <ListPlaceholder text="暂无广告位"></ListPlaceholder>
//   }
//   </div>
// </TabPane>

// <div className="mall-pro-item mb20 shadow bdr-t bdr-b bg-wt">
//   <img src={ImgProduct} alt="" className="img-home-product"/>
//   <div className="pro-info pt30 pb30 ml30">
//     <h2 className="f30 pb10">反面左半区1/2版面硬广兑换</h2>
//     <p className="f28 c-rd">第三期-发行时间：2016.11.23~2016.12.14.22</p>
//   </div>
// </div>
//
//
//
//<div className="mall-gift-item mb20 shadow bdr-t bdr-b">
//   <img src={giftImg} className="mall-gift-img"/>
//   <div className="mall-gift-info">
//     <h2 className="mall-gift-tit mb15">卡通移动电源苹果三星能用款</h2>
//     <div className="mb15"><span className="f32">1000积分/份</span> <span className="f30 del">36元</span></div>
//     <div className="exchange f24">已兑换100份 | 剩余200份</div>
//   </div>
// </div>

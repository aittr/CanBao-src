import React, { Component } from 'react';
import 'flex.css/dist/data-flex.css';
import './pick-address.less';
import Button from 'components/button/button';
import ListPlaceholder from '../modules/m-list-placeholder'

import { connect } from 'react-redux';
import { getAddressList, delAddress, setDefaultAddress} from '../../../store/actions';
import { List, SwipeAction, Modal } from 'antd-mobile'

@connect(
  state => state.address
)
export default class PickAddressPage extends Component {
  componentDidMount(){
    this.props.dispatch(getAddressList({ page:0, size:20 }))
  }
  setDefaultAddress(id){
    this.props.dispatch(setDefaultAddress({customerAddressId: id}));
  }
  editAddress(id){
    // 设置编辑项
    this.props.history.push({
      pathname: 'addAddress',
      query: { id }
    })
  }
  delAddress(id){
    Modal.alert('提示', '是否确认删除？', [
      {
        text:'取消', onPress: ()=>{},
      },{
        text:'确定', onPress: ()=>{
          this.props.dispatch(delAddress({customerAddressId: id}));
        }
    }])
  }
  createAddressItems(list){
    // 把默认选中的排前面
    return list.filter(v=>v.defualt)
      .concat(list.filter(v=>!v.default))
      .map((v, i)=>{
      return (
        <div className="view-box" key={v.id}>
          <SwipeAction
            style={{ backgroundColor: 'gray' }}
            autoClose
            right={[
              {
                text: '取消',
                onPress: () => console.log('取消'),
                style: { backgroundColor: '#ddd', color: 'white' },
              },
              {
                text: '删除',
                onPress: () =>{
                  this.delAddress(v.id);
                },
                style: { backgroundColor: '#F4333C', color: 'white' },
              },
            ]}
          >
            <div className="pick-address-item" data-flex="dir:left box:last">
              <div className="mt20 pl30">
                <div className="mb15"><span className="f32">{v.firstName}</span> <span className="f24">{v.phoneNumber}</span></div>
                <div className="c-8 f24 address-detail">{v.stateProvinceRegion} {v.city} {v.county || ''}<br/>{v.addressName || ''}</div>
              </div>
              <div className="link-setdefault" data-flex-box="0" data-flex="cross:center" onClick={()=>{this.setDefaultAddress(v.id)}}>
                <div className="f28">
                  <i className={`link-icon icon-chk-${v['default'] ? '1':'0'}`}></i>
                  默认地址
                </div>
              </div>
              <div className="link-edit" data-flex="cross:center main:center" onClick={()=>{this.editAddress(v.id)}}>
                <i className="icon-edit"></i>
              </div>
            </div>
          </SwipeAction>
        </div>
      )
    })
  }

  render () {
    const { list=[] } = this.props;
    console.log(list);

    return (
      <div ref="formBox" className="pick-address-items page-scroll-view">
        {
          list.length
          ? this.createAddressItems(list)
          : <ListPlaceholder text="暂无地址列表"></ListPlaceholder>
        }

        <div className="btn-fixed-bottom bg-wt bdr-t" style={{zIndex: 100}}>
          <Button path="addAddress">添加新地址</Button>
        </div>
    </div>
    )
  }
}

//
// <div className="view-box">
//   <SwipeAction
//     style={{ backgroundColor: 'gray' }}
//     autoClose
//     right={[
//       {
//         text: '取消',
//         onPress: () => console.log('取消'),
//         style: { backgroundColor: '#ddd', color: 'white' },
//       },
//       {
//         text: '删除',
//         onPress: () => console.log('删除'),
//         style: { backgroundColor: '#F4333C', color: 'white' },
//       },
//     ]}
//     onOpen={() => console.log('global open')}
//     onClose={() => console.log('global close')}
//   >
//   <div className="pick-address-item" data-flex="dir:left box:last">
//     <div className="mt20 pl30">
//       <div className="mb15"><span className="f32">本堂堂</span> <span className="f24">13734532998</span></div>
//       <div className="c-8 f24">上城区网尚空间</div>
//     </div>
//     <div className="link-setdefault" data-flex-box="0" data-flex="cross:center">
//       <div className="f28">
//         <i className="link-icon icon-chk-0"></i>
//         默认地址
//       </div>
//     </div>
//     <div className="link-edit" data-flex="cross:center main:center">
//       <i className="icon-edit"></i>
//     </div>
//   </div>
//   </SwipeAction>
// </div>

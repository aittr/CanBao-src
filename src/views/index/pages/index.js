import React, { Component } from 'react'
import { connect } from 'react-redux'
import Home from './home' //首页
import OrderList from './order-list' //订单
import Mall from './mall' //商城
import { Toast } from 'antd-mobile'
import { initUserInfo } from '../../../store/actions'

import icon_home0 from 'assets/img/icon-tabbar-home0.png'
import icon_home1 from 'assets/img/icon-tabbar-home1.png'
import icon_orders0 from 'assets/img/icon-tabbar-orders0.png'
import icon_orders1 from 'assets/img/icon-tabbar-orders1.png'
import icon_mall0 from 'assets/img/icon-tabbar-mall0.png'
import icon_mall1 from 'assets/img/icon-tabbar-mall1.png'

import { Tabs, TabBar } from 'antd-mobile';
import './index.less';

@connect(
  state => state.app
)
export default class MainPage extends Component {
  constructor(){
    super();
    this.state={
      hidden:false,
      selectedTab:'homeTab'
    }
  }

  callback(key) {
    // console.log(key);
  }
  componentDidMount(){
    // let userData;
    // try{ userData = JSON.parse(localStorage.userData )}catch(e){}
    // console.warn('初始化注册信息');
    // console.log(userData);
    // this.props.dispatch(initUserInfo({userInfo: userData}))
  }
  render () {
    return (
      <div className="mall-wrap">
        <TabBar
          unselectedTintColor="#5C747F"
          tintColor="#EA3C34"
          barTintColor="white"
          hidden={this.state.hidden}>
          <TabBar.Item
            title="文字料理"
            key="文字料理"
            icon= {icon_home0}
            selectedIcon= {icon_home1}
            selected={this.state.selectedTab === 'homeTab'}
            onPress={() => {
              this.setState({ selectedTab: 'homeTab'});
            }}
            data-seed="tab1">
            <Home />
          </TabBar.Item>
          <TabBar.Item
            icon={icon_mall0}
            selectedIcon={icon_mall1}
            title="积分商城"
            key="积分商城"
            selected={this.state.selectedTab === 'mallTab'}
            onPress={() => {
              if(this.props.userInfo.res === 0 ){
                return Toast.info('正在认证中，请等待');
              }
              this.setState({selectedTab: 'mallTab'});
            }}
            data-seed="tab2">
            <Mall />
          </TabBar.Item>
          <TabBar.Item
            icon={icon_orders0}
            selectedIcon={icon_orders1}
            title="订单"
            key="订单"
            selected={this.state.selectedTab === 'ordersTab'}
            onPress={() => {
              if(this.props.userInfo.res === 0 ){
                return Toast.info('正在认证中，请等待');
              }
              this.setState({selectedTab: 'ordersTab'});
            }}
            data-seed="tab3">
            <OrderList />
          </TabBar.Item>
        </TabBar>
      </div>
    )
  }
}

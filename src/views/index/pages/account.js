import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateUser } from '../../../store/actions';
import 'flex.css/dist/data-flex.css';
import './account.less'
import Input from 'components/form/input'
import Button from 'components/button/button'
import Toptip from 'components/toptip/toptip'
import { validForm, validValue } from 'Utils/lite-validator.js'
import { Link } from 'react-router'
import avatar from 'assets/img/home/avatar1.png'

@connect(
  state=>state.app,
)
export default class Account extends React.Component {
  constructor () {
    super()
    this.state = {
      errMsg: '',
      errShow:false,
      errTimmer:null,
      disabled: true,
      avatar:avatar,
      formData:{
        address:'',
        company:'',
        name:'',
        phone:'',
      }
    }
    this.updateState.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  updateState(stateName){
    return e => {
      let v;
      if(stateName === 'read'){
        v = e.target.checked;
        this.setState({disabled: !v });
      }else{
        v = e.target.value;
      }

      this.setState({
        'formData':{
          ...this.state.formData,
          [stateName]:v
        }
      })
    }
  }
  componentDidMount(){
    let userInfo = JSON.parse(JSON.stringify(this.props.userInfo));
    this.setState({
      avatar: userInfo.wximg,
      'formData':{
        address:userInfo.address,
        company:userInfo.company,
        name:userInfo.name,
        phone:userInfo.phone,
      }
    })

  }
  componentWillUnmount(){
    this.state.errTimmer && clearTimeout(this.state.errTimmer);
  }
  showWran(msg){
    this.state.errTimmer && clearTimeout(this.state.errTimmer);
    this.setState({
      errShow: true,
      errMsg: msg,
      errTimmer: setTimeout(()=>{
        this.setState({ errShow: false })
      }, 3000)
    })
  }
  valide(){
    var oFormBox = this.refs.formBoxAccount;

    return validForm(oFormBox, {
      'company':[['required'],['length:~30','字符长度过长，请小于30个字符']],
      'address': [['required'],['length:~100','字符长度过长，请小于100个字符']],
      'name': [['required'],['length:~20','字符长度过长，请小于10个字符']]
    })
  }
  handleSubmit() {
    this.valide().then(res=>{
      let pms = this.state.formData;
      pms.phone = this.props.userInfo.phone;
      console.log('注册提交：')
      console.log(pms)
      this.props.dispatch(updateUser(pms));
    }).catch(err=>{
      if(err instanceof Error) throw err;
      err.el.focus();
      err.el.parentElement.classList.add('error');
      this.showWran(err.msg || '输入信息不正确');
    })
  }
  handleIptClick(e){
    e.target.parentElement.classList.remove('error')
  }
  render () {
    let { userType } = this.props;
    console.log(this.props)
    let userInfo = this.state.formData;
    if(!userInfo.company) return null;
    console.log(this.state.avatar)
    return (
      <div ref="formBoxAccount" className="account-frm pt20">
        <div className="tc pb25 mt5">
          <i className="bg-center bg-avatar1" style={{backgroundImage:`url(${this.state.avatar})`}}></i>
        </div>
        <div className="ui-ipt-group view-box" data-flex="">
          <label className="ui-ipt-label bdr-r label-icon" data-flex-box="0">账号</label>
          <input type="text" className="ui-ipt" name="company" readOnly value={userInfo.company} placeholder="" data-flex-box="1"
            onChange = { this.updateState('companyName') }
            onClick = { this.handleIptClick }
          />
        </div>
        <div className="ui-ipt-group view-box" data-flex="">
          <label className="ui-ipt-label bdr-r label-icon" data-flex-box="0">地址</label>
          <input type="text" className="ui-ipt" name="address" readOnly value={userInfo.address} placeholder="" data-flex-box="1"
            onChange = { this.updateState('address') }
            onClick = { this.handleIptClick }
            />
        </div>
        <div className="ui-ipt-group view-box" data-flex="">
          <label className="ui-ipt-label bdr-r label-icon" data-flex-box="0">姓名</label>
          <input type="text" className="ui-ipt" name="name" value={userInfo.name}  placeholder="" data-flex-box="1"
            onChange = { this.updateState('name') }
            onClick = { this.handleIptClick }
            />
        </div>

        <div className="ui-ipt-group view-box" data-flex="">
          <label className="ui-ipt-label bdr-r label-icon" data-flex-box="0">手机号</label>
          <input type="text" className="ui-ipt" name="phone" value={userInfo.phone} readOnly placeholder="请点击右则认证手机号" data-flex-box="1" />
          <Link to="editPhone" className="ui-ipt-edittip c-8">{userInfo.phone ? '修改':'认证'}<i className="bg-center icon-arrow-right"></i></Link>
        </div>

        <div className="ui-ipt-group view-box" data-flex="main:justify">
          <label className="ui-ipt-label bdr-r label-icon" data-flex-box="0">证件照</label>
          <div className="btn-send-msg mr10 pt5">
            <Link to="authen" className="ui-btn bdr primary dib">{this.props.userInfo.status}</Link>
          </div>
        </div>

        <div className="ui-ipt-group view-box" data-flex="main:justify">
          <label className="ui-ipt-label bdr-r label-icon" data-flex-box="0">关于我们</label>
          <div className="btn-read pt15 mr20">
            <Link to="about" className="c-8 f28">查看<i className="bg-center icon-arrow-right"></i></Link>
          </div>
        </div>

        <div className="btn-fixed-bottom">
          <Button className="mb20"
            onClick={this.handleSubmit.bind(this)}
          >确认</Button>
        </div>

        <Toptip show={this.state.errShow} type="warn">{this.state.errMsg}</Toptip>
    </div>
    )
  }
}
// <Button onClick="">返回</Button>

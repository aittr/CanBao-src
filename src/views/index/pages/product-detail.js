import React, { Component } from 'react';
import 'flex.css/dist/data-flex.css';
import './product-detail.less'
import Button from 'components/button/button'
import ImgProduct from 'assets/img/home/6.png'
import moment from 'moment'
import { Toast } from 'antd-mobile'
import { connect } from 'react-redux'
import { getProductDetail } from '../../../store/actions'

@connect(
  state => state
)
export default class ProductPage extends Component {
  constructor(){
    super();
    this.state={
      checkedNum:null,
    }
    this.createNumbers = this.createNumbers.bind(this);
    this.checkedNumsEv = this.checkedNumsEv.bind(this);
  }
  componentDidMount(){
    const prodId = this.props.location.query.prodId;
    console.log(prodId)
    this.props.dispatch(getProductDetail({prodId: prodId}))
  }
  checkedNumsEv(num){
    this.setState({
      checkedSkuId: num
    })
  }
  createNumbers(numbers){
    return numbers.map((v,i)=>{
      return (
        <div className="pro-numbers" key={i}>
          <a href="javascript:;" className={`pro-numbers-item db bdr ${this.state.checkedSkuId === v.skuId ? 'active':''}`} onClick={()=>{this.checkedNumsEv(v.skuId)}}>
            <div className="f26 mb5">{v.optKey}</div>
            <div className="f20">{ moment(v.startTs).format('MM-DD') } - {moment(v.endTs).format('MM-DD')}</div>
          </a>
        </div>
      )
    })
  }
  wait(e){
    e.preventDefault();
    Toast.info('功能开发中...');
    return false;
  }
  render () {
    const { proNumbers=[], list=[] }= this.props.product;
    let prodId = ~~this.props.location.query.prodId;
    let isSpecial = ~~this.props.location.query.isSpecial;
    const { userInfo } = this.props.app;
    let proDetail;
    if(isSpecial){
      proDetail = this.props.app.specialProduct;
    }else{
      proDetail = list.filter(v=>v.id === prodId)[0];
    }

    console.log(proDetail);
    console.log(proNumbers);

    return (
      <div className="product-detail-wrap page-scroll-view">
        <div className="pro-detail-info mb20 bg-wt">
          <img src={proDetail.skuMedia.primary.url} className="pro-detail-img"/>
          <div className="tit" data-flex="main:justify">
            <div className="f32">{proDetail.name}</div>
            <div><span className="f32">3000</span> <span className="24">积分/期 <span className="del">1000</span></span> </div>
          </div>
        </div>

        <div className="view-box">
          <div className="pl30 pr30 pro-pay-info">
            <div className="f32 bdr-b" data-flex="main:justify">
              <div>当前积分：<span>{userInfo.point}</span></div>
              <div>共需支付：<span className="c-rd">---</span></div>
            </div>
            <div className="c-8 f28" data-flex="main:justify">
              <span>已租36期</span>
              <span>200mm*275mm</span>
            </div>
          </div>
        </div>

        <div className="view-box pt10 pb10">
          <div className="tc">
            <p className="f32">请选择租期</p>
            <p className="c-rd f28">(可享9折优惠)</p>
          </div>
          <div className="pt20">
            {
              this.createNumbers(proNumbers)
            }
          </div>
        </div>

          <div className="pro-detail-desc view-box">
            <div className="inner pl30">
              <div className="tit bdr-b f30 pt20 pb20">广告位描述：</div>
              <p className="con f26 c-8 pt20 pb20">
              测试文字本广告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告
              </p>
            </div>
          </div>

        <div className="btn-fixed-bottom bg-wt bdr-t">
          <Button onClick={this.wait}>立即订购</Button>
        </div>
      </div>
    )
  }
}

// <div className=""><Button size="small">第四期</Button></div>

// <div className="product-detail-wrap pt20 page-scroll-view">
//   <div className="pro-detail-info mb20 bg-wt">
//     <img src={ImgProduct} alt="" className="pro-detail-img"/>
//     <div className="tit" data-flex="main:justify">
//       <div className="f32">正面左半区广告位（大）</div>
//       <div><span className="f32">900</span> <span className="24">积分/期 <span className="del">1000</span></span> </div>
//     </div>
//   </div>
//
//   <div className="view-box">
//     <div className="pl30 pr30 pro-pay-info">
//       <div className="f32 bdr-b" data-flex="main:justify">
//         <div>当前积分：<span>3000</span></div>
//         <div>共需支付：<span className="c-rd">---</span></div>
//       </div>
//       <div className="c-8 f28" data-flex="main:justify">
//         <span>已租36期</span>
//         <span>200mm*275mm</span>
//       </div>
//     </div>
//   </div>
//
//   <div className="view-box pt10 pb10">
//     <div className="tc">
//       <p className="f32">请选择租期</p>
//       <p className="c-rd f28">(可享9折优惠)</p>
//     </div>
//     <div>
//       {
//
//       }
//     </div>
//   </div>
//
//   <div className="pro-detail-desc view-box">
//     <div className="inner pl30">
//       <div className="tit bdr-b f30 pt20 pb20">广告位描述：</div>
//       <div className="con f28 c-8 pt20 pb20">
//         本广告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告告位为“文字料理”正面左半部分广告
//       </div>
//     </div>
//   </div>
//
//   <div className="btn-fixed-bottom bg-wt bdr-t">
//     <Button>立即订购</Button>
//   </div>
// </div>

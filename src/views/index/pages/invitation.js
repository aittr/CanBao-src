import React from 'react'
import './invitation.less'
import 'assets/css/icon/ui-icons.less'
import Button from 'components/button/button'
import { connect } from 'react-redux'
import { getInvitaionCode } from '../../../store/actions'
import { Toast } from 'antd-mobile'
import { hashHistory } from 'react-router'
import shareImg from 'assets/img/share_img.jpg';
import Share from 'components/share'

const SHARE_CONFIG={
  title: '', // 分享标题
  link: '', // 分享链接
  imgUrl: '', // 分享图标
  success: function () { Toast.success('分享成功', 3); }
}

// 邀请人姓名  邀请您成为“文字料理”的分发商啦！
// 注册输入邀请码“******”，双方均可获得**积分，点击注册吧！

@connect(
  state=>state.app
)
export default class InvitationPage extends React.Component {
  constructor(){
    super();
    // this.state={ show: false }
  }
  componentDidMount(){
    this.props.dispatch(getInvitaionCode())
    wx.onMenuShareAppMessage(SHARE_CONFIG)
    wx.onMenuShareQQ(SHARE_CONFIG)
    wx.onMenuShareTimeline(SHARE_CONFIG)
  }
  // showMask(){
  //   this.setState({ show:true })
  // }
  // closeShareMask(){
  //   this.setState({ show: false })
  // }
  render () {
    const { invitationInfo='', userInfo={} } = this.props;
    // 更新一下分享的链接，带上 invitationCode  邀请码******注册“文字料理”，立即获得688积分（约48元）！
    SHARE_CONFIG.link = location.origin+'/#/register?type=1&inCode='+invitationInfo.invitationId;
    // SHARE_CONFIG.title = `${userInfo.name}邀请您成为"文字料理"的分发商啦！注册输入邀请码"${invitationInfo.invitationId}"，双方均可获得${invitationInfo.reg_point}积分，点击注册吧！`;
    SHARE_CONFIG.title = `邀请码${invitationInfo.invitationId}注册“文字料理”，立即获得${invitationInfo.reg_point}积分（约${Math.floor(invitationInfo.reg_point/100*7)}元）！`;
    SHARE_CONFIG.imgUrl =location.origin + shareImg;

    return (
      <div>
        <div className="invitation-top tc c-wt bg-rd">
          <div className="points-box dib">
            <div className="points-txt">
              <div className="f30">当前积分</div>
              <div className="points">{userInfo.point}</div>
            </div>
          </div>
          <h2 className="tit">邀请送积分</h2>

          <table className="steps tl f26">
            <tbody>
              <tr><td>Step1：</td><td>将您的邀请码分享给好友</td></tr>
              <tr><td>Step2：</td><td>好友注册填写邀请码并加入分发商，好友可立即获得{invitationInfo.reg_point}积分</td></tr>
              <tr><td>Step3：</td><td>等待分发商审核完成并且参与分发</td></tr>
              <tr><td>Step4：</td><td>第一次分发结束后，您可获得{invitationInfo.reg_point}积分（价值约{Math.floor(invitationInfo.reg_point/100*7)}元）</td></tr>
            </tbody>
          </table>
        </div>

        <div className="invitation-send-box tc">
            <a href="javascript:;" className="f30 c-rd">点击右上角菜单分享您的邀请码</a>
            <div className="f30 mt30 b">{invitationInfo.invitationId || '无'}</div>
        </div>

      </div>
    )
  }
}


// <div className="p20 invitation-btnbox">
//   <Button className="btnauth" onClick={()=>{this.showMask()}}>分享</Button>
// </div>
// <Share show={this.state.show} close={()=>{this.closeShareMask()}}></Share>

// <ul data-flex="dir:left box:mean" className="type-icons">
//   <li className="send-type-item" onClick={()=>{wx.onMenuShareAppMessage(SHARE_CONFIG)}}>
//     <i className="icon70 icon-weixin"></i>
//     <p className="f26 mt15 c-8">分享给朋友</p>
//   </li>
//   <li className="send-type-item" onClick={()=>{wx.onMenuShareQQ(SHARE_CONFIG)}}>
//     <i className="icon70 icon-qq"></i>
//     <p className="f26 mt15 c-8">分享给QQ好友</p>
//   </li>
//   <li className="send-type-item" onClick={()=>{wx.onMenuShareTimeline(SHARE_CONFIG)}}>
//     <i className="icon70 icon-pyq"></i>
//     <p className="f26 mt15 c-8">分享至朋友圈</p>
//   </li>
// </ul>

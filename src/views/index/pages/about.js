import React, { Component } from 'react';
import './about.less'

export default class Abount extends React.Component {
  render () {

    return (
      <div className="about-wrap page-scroll-view">
        <div className="p20 inner">
        <h2 className="tit">文字料理是什么？</h2>
        <p className="con">
        文字料理是一份有思想、有个性、有颜、更有料的用餐阅读物。文字料理将伴随白领外卖一起来到顾客的手中，不仅可以让你的午餐时间不再枯燥，更可以用作餐垫让餐后整理更加便捷。你可以在这里：
        阅读到好玩、有趣、新鲜的内容，了解一手活动资讯
        通过公众号告诉我们你想看的内容，评论吐槽已有的内容和话题
        这里不仅有趣味内容，还有不定时惊喜大礼随机送出
        </p>
        <h2 className="tit">
        总之，你们的午餐时间，我们包了！
        </h2>
        <h2 className="tit">我们的顾客在哪里？</h2>
        <p className="con">
        文字料理将成为每份白领外卖的标配之一，精准覆盖中国2亿白领，在一个高频次、低干扰的环境内与顾客高效互动。
        </p>
        </div>
      </div>
    )
  }
}

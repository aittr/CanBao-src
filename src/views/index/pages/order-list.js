import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getOrders, confirmOrder } from '../../../store/actions'
import moment from 'moment'
import ListPlaceholder from '../modules/m-list-placeholder'
import ImgProduct from 'assets/img/home/5.png'

import 'flex.css/dist/data-flex.css'
import './order-list.less'
import Button from 'components/button/button'

import { Tabs } from 'antd-mobile'
const TabPane = Tabs.TabPane;

@connect(
  state => state.order
)
export default class RechargePage extends Component {
  constructor(){
    super();
    this.confirmOrderEv = this.confirmOrderEv.bind(this);
  }
  callback(key) {
    console.log(key);
  }
  componentDidMount(){
    this.props.dispatch(getOrders({ page: 0, size:20 }));
  }
  confirmOrderEv(id){
    this.props.dispatch(confirmOrder({ distId: id }));
  }
  render () {
    const { list = [] } = this.props;
    // <img src={v.skuMedia.primary.url+'?imageslim&imageView2/1/w/750/h/180'} alt="" className="img-home-product"/>

    const proItems = list.map((v,i)=>{
      return (
        <div className="order-pro-list-item view-box" key={v.id}>

            <div className="mb30" data-flex="">
              <div className="order-pro-img bdr mr30 mb30 bg-center" style={{backgroundImage:'url('+ImgProduct+')'}}></div>
              <div className="order-pro-info pct100">
                <div className="f30 b">{v.attributeValue}</div>
                <div className="f24 c-8">{v.count}份</div>
                <div className="f26 c-8 mt25 mb5">要求发行时间</div>
                <div className="f24 c-8">{ moment(v.activeStartDate).format('YYYY-MM-DD') } - { moment(v.activeEndDate).format('YYYY-MM-DD') }</div>
                <div className="f30 tr c-rd mb20 mt10">+{v.point}积分</div>
              </div>
            </div>

            <div className="order-flow rel">
              <div className="order-states bdr-b" data-flex="dir:left box:mean">
                <span className={`order-state-item state1 ${v.status>=1?'active':''}`}>已制作</span>
                <span className={`order-state-item state2 ${v.status>=2?'active':''}`}>已派送</span>
                <span className={`order-state-item state3 ${v.status>=3?'active':''}`}>已收货</span>
                <span className={`order-state-item state4 ${v.status>=4?'active':''}`}>分发完成</span>
              </div>
              <div className="order-state-btn" style={{left:`${(v.status*2-1)/8*100}%`}}><Button size="small">{v.attributeValue}</Button></div>
            </div>
            <div className="tr bdr-t pt20" style={{display: v.status==2 ? 'block': 'none'}}>
                <Button type="primary" className="order-btn-confirm" onClick={()=>this.confirmOrderEv(v.id)}>确认收货</Button>
            </div>
        </div>
      )
    })

    return (
      <div className="order-list-wrap pb75">
        <div>
          {
            list.length
            ? proItems
            : <ListPlaceholder text="暂无分发数据"></ListPlaceholder>
          }
        </div>
      </div>
    )
  }
}

// <TabPane tab="我的购买" key="2" className="mt20">
//   <div>
//      <ListPlaceholder text="暂无购买记录"></ListPlaceholder>
//   </div>
// </TabPane>

// <div className=""><Button size="small">第四期</Button></div>
// <div className="" data-flex="">
//   <div className="order-pro-info pct100">
//     <div className="mb15"><span className="f32 b">{v.attributeValue}</span><span className="f24 c-8"> {v.count}份</span></div>
//     <div><span className="f30 c-8 mt25 mb5">要求发行时间: </span><span className="f26 c-8">{ moment(v.activeStartDate).format('YYYY-MM-DD') } - { moment(v.activeEndDate).format('YYYY-MM-DD') }</span></div>
//     <div className="f30 tr c-rd mb20 mt10">+{v.point}积分</div>
//   </div>
// </div>

// <div className="order-pro-list-item bdr-t bdr-b shadow">
//     <div className="" data-flex="">
//       <div className="order-pro-img bdr mr30 mb30"></div>
//       <div className="order-pro-info">
//         <div className="f32 b">文字料理第四期</div>
//         <div className="f24 c-8">3000份</div>
//         <div className="f30 c-8 mt25 mb5">要求发行时间</div>
//         <div className="f24 c-8">2016.10.10 - 12.20</div>
//       </div>
//     </div>
//     <div className="order-flow">
//       <div className="order-states" data-flex="dir:left box:mean">
//         <span className="order-state-item state1">已制作</span>
//         <span className="order-state-item state2">已派送</span>
//         <span className="order-state-item state3">已收货</span>
//         <span className="order-state-item state4">分发完成</span>
//       </div>
//     </div>
//     <div className="tr bdr-t pt20">
//         <Button type="primary" className="order-btn-confirm">确认收货</Button>
//     </div>
// </div>
//


//
// <div className="order-pro-list-item bdr-t bdr-b shadow">
//     <div className="" data-flex="">
//       <div className="order-pro-img bdr mr30 mb30"></div>
//       <div className="order-pro-info">
//         <div className="f32 b">文字料理第四期</div>
//         <div className="f24 c-8">10月24日 17：30下单</div>
//         <div className="f30 c-8 mt25 mb5">购买第三期与第五期</div>
//         <div className="f24 c-8">2016.10.10 - 12.20 | 2016.11.10 - 12.10</div>
//       </div>
//     </div>
//     <div className="order-flow">
//       <div className="order-states" data-flex="dir:left box:mean">
//         <span className="order-state-item state1">已订购</span>
//         <span className="order-state-item state2">已编辑</span>
//         <span className="order-state-item state3">已发行</span>
//         <span className="order-state-item state4">完成</span>
//       </div>
//     </div>
//     <div className="tr bdr-t pt20">
//       <Button className="order-btn-confirm">删除订单</Button>
//       <Button className="order-btn-confirm">查看更多</Button>
//       <Button type="primary" className="order-btn-confirm">分发监控</Button>
//     </div>
// </div>

import React, { Component } from 'react'
import 'flex.css/dist/data-flex.css'
import './withdraw.less'
import { Toast } from 'antd-mobile'
import { connect } from 'react-redux'
import { getMount, submitWithdraw } from '../../../store/actions'
import Button from 'components/button/button'
import '../../../components/form/input.css'

@connect(
  state => state.app
)
export default class WithdrawPage extends Component {
  constructor(){
    super();
    this.state = {
      checkedMount:0
    }
    this.submitWithdrawEv = this.submitWithdrawEv.bind(this);
    this.checkMountEv = this.checkMountEv.bind(this);
  }
  componentDidMount(){
    this.props.dispatch(getMount())
  }
  formatPoint(point){
    return point>=10000 ? point/10000 + 'w' : point;
  }
  getMoneyByPoint(point){
    return point/100*7;
  }
  checkMountEv(mount){
    this.setState({
      checkedMount: mount
    })
  }
  submitWithdrawEv(){
    if(this.props.userInfo.point<2000){
      return Toast.info('积分不足')
    }
    this.props.dispatch(submitWithdraw({amount:this.state.checkedMount}))
  }
  render () {
    const {userInfo={}, mountList=[]} = this.props;

    const checkedMount = this.state.checkedMount;
    console.log(mountList);

    const mountListTpl = mountList.map((v, i)=>{
      return (
        <div className="p5 pct25" key={i}><Button className={`withdraw-point-item ${v === checkedMount?'active':''}`} onClick={()=>{this.checkMountEv(v)}}>{this.formatPoint(v)}积分</Button></div>
      )
    })

    return (
      <div className="recharge-wrap pt20 page-scroll-view">
        <div className="recharge-form bdr-b bdr-t shadow bg-wt">
          <div className="inner">
            <div className="f30 bdr-b">当前积分：<span className="c-rd">{userInfo.point}积分</span></div>
            <div className="f30">提现金额(元): <span className="c-8">{checkedMount>0?`${this.getMoneyByPoint(checkedMount)}元`:'请选择'}</span></div>
          </div>
        </div>
        <div className="recharge-points">
          <p className="c-8 mt20 f28 ml30 pl20">提现起步积分：2000积分 <span>(100积分=7元)</span></p>
          <div className="m30" data-flex="box:mean">
            {mountListTpl}
          </div>
        </div>
        <div className="recharge-to-wx bdr-t bdr-b shadow bg-wt">
          <div className="inner">
            <div className="f30 bdr-b lh90 tip"><span className="f28 b">提现至本微信</span><span className="c-8 f20">（我们将以红包形式发至账号所绑定的微信）</span></div>
            <div className="f30 pt10 pb10" data-flex="dir:left box:justify">
              <div>
                <i className="icon-wxzhifu"></i>
              </div>
              <div className="mt20">
                <div className="f34">微信</div>
                <p className="c-8 f28">推荐安装微信5.0以上版本使用</p>
              </div>
              <div data-flex="cross:center">
                <i className="icon-chk-1 icon40"></i>
              </div>
            </div>
          </div>
        </div>

        <div className="btn-fixed-bottom">
          <Button onClick={this.submitWithdrawEv} disabled={this.state.checkedMount===0}>提交申请</Button>
        </div>
      </div>
    )
  }
}

// <div className="ui-ipt-group bdr-t" data-flex="">
//   <label className="ui-ipt-label bdr-r" data-flex-box="0">提现金额(元):</label>
//   <input type="text" className="ui-ipt" name="company" placeholder="请选择" data-flex-box="1" />
// </div>

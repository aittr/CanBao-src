import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestRegister } from '../../../store/actions';
import '../../../components/form/input.css'
import 'flex.css/dist/data-flex.css';
import './edit-phone.less'
import Input from 'components/form/input'
import Button from 'components/button/button'
import Toptip from 'components/toptip/toptip'
import { validForm, validValue } from 'Utils/lite-validator.js'

export default class EditEmailPage extends React.Component {
  constructor () {
    super()
    this.state = {
      errMsg: '',
      errShow:false,
      errTimmer:null,
      disabled: true,
      formData:{
        address:'',
        company:'',
        imgs:'',
        invitationCode:'',
        name:'',
        phone:'',
        read:'',
      }
    }
    this.updateState.bind(this);
  }
  updateState(stateName){
    return e => {
      let v;
      if(stateName === 'read'){
        v = e.target.checked;
        this.setState({disabled: !v });
      }else{
        v = e.target.value;
      }

      if(!validValue.required(v) || v === this.state.formData[stateName]) return;
      this.setState({
        'formData':{
          ...this.state.formData,
          [stateName]:v
        }
      })
    }
  }
  componentWillUnmount(){
    this.state.errTimmer && clearTimeout(this.state.errTimmer);
  }
  showWran(msg){
  }
  valide(){
  }
  handleSubmit() {
  }
  handleIptClick(e){
    // e.target.parentElement.classList.remove('error')
  }
  render () {
    return (
      <div ref="formBox" className="edit-phone-frm pt20">
        <div className="shadow">
          <div className="ui-ipt-group bdr-t" data-flex="">
            <label className="ui-ipt-label bdr-r" data-flex-box="0">新邮箱地址</label>
            <input type="text" className="ui-ipt" name="company" placeholder="请输入新邮箱地址" data-flex-box="1" />
            <div className="btn-send-msg bdr-l tc pt5">
              <a className="ui-btn bdr primary" href="javascript:;">发送</a>
            </div>
          </div>
          <div className="ui-ipt-group bdr-b ui-form-line" data-flex="">
            <label className="ui-ipt-label bdr-r" data-flex-box="0">校验码</label>
            <input type="text" className="ui-ipt" name="company" placeholder="请输入邮箱校验码" data-flex-box="1" />
          </div>
        </div>

        <div className="btn-edit-phone-box p20" style={{paddingTop:'60px'}}>
          <Button
            onClick={this.handleSubmit.bind(this)}
            disabled={this.state.disabled}
          >确认</Button>
        </div>

        <Toptip show={this.state.errShow} type="warn">{this.state.errMsg}</Toptip>
    </div>
    )
  }
}


// <Input type="text" name="company" placeholder="请输入新手机号码" label="新手机号"></Input>
// <Input type="text" name="address" placeholder="请输入短信校验码" label="校验码"></Input>

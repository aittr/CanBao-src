import '../../assets/css/base.less'
import 'assets/css/nprogress.less'
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configStore from '../../store'
import routes from './routes'
import Api from '../../api'
// import 'vconsole'

// 初始化微信JSSDK
Api.getWxConfig({
  url: location.href.split('#')[0]
}).then(res=>{
  if(res.errorCode === 1500) return;
  var data = res.data;

  wx.config({
    "appId": data.appId,
    "debug": false,
    "jsApiList": data.jsApiList,
    "nonceStr": data.nonceStr,
    "signature": data.signature,
    "timestamp": data.timestamp
  })
  wx.ready(res=>console.log('wx-init_ok',res));
  wx.error(err=>console.log('wx-init-error', err));
}, err=>{
  console.log(err);
})

let store = configStore();
window._REDUX_STORE = store;

ReactDOM.render(
  <Provider store={ store}>
    { routes }
  </Provider>,
  document.getElementById('root'));

import Api from './api';

const DEBUG = false;
// DEBUG ? 'http://rap.taobao.org/mockjsdata/11220/api' :

const api = new Api({
  baseURI: 'http://test.51aogu.com/tuluo/api',
  // baseURI: 'http://test.51aogu.com/tuluo/api',
  timeout: 60000, //超时时间1分钟
  withCredentials: true,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded'
  }
})

export default  {
  getUser: api.get('/home'),
  createUser: api.post('/register', { loading: true }),
  editUser: api.post('/member/update', { loading: true }),
  reflashUserInfo: api.get('/member'),
  getRegPoint: api.get('/getRegPoint'),

  getInviCode: api.get('/invitationCode'),
  getPoint4User: api.get('/getPoint'),
  getMount: api.get('/getmount'),
  submitWithdraw: api.get('/withdraw', { loading: true }),
  getPoints: api.get('/pointDetail'),

  uploadImgs: api.post('/updateImgs'),
  getImgs: api.post('/getImgs'),

  sendVCode: api.get('/sendOphone', { loading: true }),
  // 更新手机号
  updatePhone: api.get('/verCode', { loading: true }),
  // 注册验证手机号
  verCode4Reg: api.get('/verCodeForReg', { loading: true }),

  getOrders: api.get('/getDirt'),
  confirmOrder: api.get('/confirm', { loading: true }),

  getProducts: api.get('/allProduct'),
  getSpecialProduct: api.get('/getSpecial'),
  // getSpecialProduct: api.get('/getProduct'),
  getProductDetail: api.get('/ProductSku'),

  getAddressList: api.get('/address/view'),
  createAddress: api.post('/address/create', { loading: true }),
  updateAddress: api.post('/address/update', { loading: true }),
  delAddress: api.get('/address/delete', { loading: true }),
  setDefaultAddress: api.get('/address/default', { loading: true }),
  getCities: api.get('/cities'),

  // 获取微信权限配置
  getWxConfig: api.get('/../wx/getConfig'),
  // 地心小编
  getNotice: api.get('/dynamic'),
}

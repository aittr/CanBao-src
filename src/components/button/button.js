import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';
import './button.css'

export default class Button extends React.Component {
    static propTypes = {
        disabled: React.PropTypes.bool,
        /**
         * Options: default, primary, warn, plain
         *
         */
        type: React.PropTypes.string,
        /**
         * Options: normal, small
         *
         */
        size: React.PropTypes.string,
        path: React.PropTypes.string,
    };

    static defaultProps = {
        disabled: false,
        type: 'default',
        size: 'normal',
        path: '',
    };

    render() {
        const { type, size, className, children,path, ...others } = this.props;
        const cls = classNames({
            'ui-btn bdr': true,
            'mini': size === 'small',
            'primary': type === 'primary',
            'default': type === 'default',
            'warn': type === 'warn',
            'plain': type == 'plain',
            'disabled': this.props.disabled,
            [className]: className
        });

        return (
            path ? <Link to={path} { ...others } className={ cls }>{children}</Link> :
            <a { ...others } className={ cls } href="javascript:;">{ children }</a>
        );
    }
};

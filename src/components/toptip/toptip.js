import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import Animate from 'rc-animate';
import './toptip.css'

/**
 *  Drop down message from top
 *
 */
const Toptips = (props) => {
    const {className, type, children, show, ...others} = props;
    const cls = classNames({
        'weui-toptips': true,
        [`weui-toptips_${type}`]: true,
        [className]: className
    });

    return (
      <div>
        <Animate
           transitionName="fade"
           transitionAppear
         >
         {show ? <div className={cls} {...others}>
             {children}
         </div> : null}
      </Animate>
    </div>
    );
};

Toptips.propTypes = {
    show: React.PropTypes.bool,
    type: React.PropTypes.string
};

Toptips.defaultProps = {
    show: false,
    type: 'default'
};

export default Toptips

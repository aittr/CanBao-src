import Form from './form';
import FormCell from './form_cell';
import TextArea from './textarea';
import Input from './input';
import Select from './select';
import Uploader from './uploader';

export default {
    FormCell,
    Input,
    Radio,
    Checkbox,
    Select,
    Uploader,
};

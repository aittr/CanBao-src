import React from 'react';
import classNames from 'classnames';
import 'flex.css/dist/data-flex.css';
import { Link } from 'react-router';
import './input.css'

const Input = (props) => {
    const {className, label, labelIcon, editPath, ...others} = props;
    const labelCls = classNames('ui-ipt-label bdr-r', {'label-icon': labelIcon})
    const cls = classNames({
        'ui-ipt-group': true,
        'ui-ipt-edit': !!editPath,
        [className]: className
    });

    return (
        <div className="ui-ipt-group bdr-t bdr-b mb20 shadow" data-flex="">
            <label className={labelCls} data-flex-box="0">{ label }</label>
            <input className="ui-ipt" { ...others }  data-flex-box="1"/>
            {editPath ? <Link to={editPath} className="ui-ipt-edittip c-8">修改<i className="bg-center icon-arrow-right"></i></Link> : null}
        </div>
    );
};
Input.propTypes = {
    labelIcon: React.PropTypes.bool,
    editPath: React.PropTypes.string,
}

Input.defaultProps = {
    labelIcon: false,
    editPath: '',
}

export default Input

import React from 'react';
import classNames from 'classnames';
import 'assets/css/icon/weui-icon_font.less'
import './checkbox.css'

/**
 * weui wrapper for checkbox
 *
 */
const Checkbox = (props) => {
    const { className, ...others } = props;
    const cls = classNames({
        'weui-check': true,
        [className]: className
    });

    return (
        <span>
            <input className={cls} type="checkbox" {...others}/>
            <span className="weui-icon-checked"></span>
        </span>
    );
};

export default Checkbox

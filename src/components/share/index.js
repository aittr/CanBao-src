import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import './share.less'

const Share = (props) => {
    const {show,close, ...others} = props;

    return (
      <div className="wx-share-mask" style={{display:show?'block':'none'}}
        onClick={close}
      >
        <i className="wx-share-icon"></i>
        <div className="wx-share-box">
          点击右上角发送给指定朋友或分享到朋友圈
        </div>
      </div>
    );
};

Share.propTypes = {
    show: React.PropTypes.bool,
    close: React.PropTypes.func
};

Share.defaultProps = {
    show: false,
    close: Function()
};

export default Share

import { createAction } from 'redux-actions'

// GOTO & toast & alert ???
export const GOPAGE = 'GOPAGE';
export const goPage = createAction(GOPAGE);

//user
export const SET_USER_TYPE = 'SET_USER_TYPE';
export const setUserType = createAction(SET_USER_TYPE);
export const REQUEST_GET_USER = 'REQUEST_GET_USER';
export const SUCCESS_GET_USER = 'SUCCESS_GET_USER';
export const FAILURE_GET_USER = 'FAILURE_GET_USER';
export const requestGetUser = createAction(REQUEST_GET_USER);
export const successGetUser = createAction(SUCCESS_GET_USER);
export const REQUEST_CREATE_USER = 'REQUEST_CREATE_USER';
export const SUCCESS_CREATE_USER = 'SUCCESS_CREATE_USER';
export const FAILURE_CREATE_USER = 'FAILURE_CREATE_USER';
export const requestCreateUser = createAction(REQUEST_CREATE_USER);
export const successCreateUser = createAction(SUCCESS_CREATE_USER);
export const failureCreateUser = createAction(FAILURE_CREATE_USER);
export const UPDATE_USER = 'UPDATE_USER'
export const UPDATE_USER_OK = 'UPDATE_USER_OK'
export const updateUser = createAction(UPDATE_USER);
export const updateUserOK = createAction(UPDATE_USER_OK);

// send vcode
export const SEND_VCODE = 'SEND_VCODE'
export const sendVCode = createAction(SEND_VCODE)
export const UPDATE_PHONE = 'UPDATE_PHONE'
export const updatePhone = createAction(UPDATE_PHONE)

export const INIT_USER_INFO = 'INIT_USER_INFO'
export const initUserInfo = createAction(INIT_USER_INFO)

// imgs
export const GET_IMGS = 'GET_IMGS'
export const getImgs = createAction(GET_IMGS)
export const UPLOAD_IMGS = 'UPLOAD_IMGS'
export const uploadImgs = createAction(UPLOAD_IMGS)


// orders
export const GET_ORDERS = 'GET_ORDERS';
export const GET_ORDERS_OK = 'GET_ORDERS_OK';
export const getOrders = createAction(GET_ORDERS);
export const getOrdersOk = createAction(GET_ORDERS_OK);
export const CONFIRM_ORDER = 'CONFIRM_ORDER';
export const CONFIRM_ORDER_OK = 'CONFIRM_ORDER_OK';
export const confirmOrder = createAction(CONFIRM_ORDER);
export const confirmOrderOK = createAction(CONFIRM_ORDER_OK);

// invitation code
export const GET_INVITATION_CODE = 'GET_INVITATION_CODE';
export const GET_INVITATION_CODE_OK = 'GET_INVITATION_CODE_OK';
export const getInvitaionCode = createAction(GET_INVITATION_CODE);
export const getInvitaionCodeOK = createAction(GET_INVITATION_CODE_OK);

// point
export const GET_POINT_FOR_USER = 'GET_POINT_FOR_USER';
export const GET_POINT_FOR_USER_OK = 'GET_POINT_FOR_USER_OK';
export const getPoint4User = createAction(GET_POINT_FOR_USER);
export const getPoint4UserOK = createAction(GET_POINT_FOR_USER_OK);
export const GET_POINTS = 'GET_POINTS';
export const GET_POINTS_OK = 'GET_POINTS_OK';
export const getPoints = createAction(GET_POINTS);
export const getPointsOK = createAction(GET_POINTS_OK);

// product
export const GET_PRODUCTS = 'GET_PRODUCTS'
export const GET_PRODUCTS_OK = 'GET_PRODUCTS_OK'
export const getProducts = createAction(GET_PRODUCTS)
export const getProductsOK = createAction(GET_PRODUCTS_OK)
export const GET_PRODUCT_DETAIL = 'GET_PRODUCT_DETAIL'
export const GET_PRODUCT_DETAIL_OK = 'GET_PRODUCT_DETAIL_OK'
export const getProductDetail = createAction(GET_PRODUCT_DETAIL)
export const getProductDetailOK = createAction(GET_PRODUCT_DETAIL_OK)
export const GET_SPECIAL_PRODUCT = 'GET_SPECIAL_PRODUCT'
export const getSpecialProduct = createAction(GET_SPECIAL_PRODUCT)

// address
export const GET_ADDRESS_LIST = 'GET_ADDRESS_LIST'
export const GET_ADDRESS_LIST_OK = 'GET_ADDRESS_LIST_OK'
export const getAddressList = createAction(GET_ADDRESS_LIST)
export const getAddressListOK = createAction(GET_ADDRESS_LIST_OK)
export const CREATE_ADDRESS = 'CREATE_ADDRESS'
export const CREATE_ADDRESS_OK = 'CREATE_ADDRESS_OK'
export const createAddress = createAction(CREATE_ADDRESS)
export const createAddressOK = createAction(CREATE_ADDRESS_OK)
export const GET_CITIES = 'GET_CITIES'
export const GET_CITIES_OK = 'GET_CITIES_OK'
export const getCities = createAction(GET_CITIES)
export const getCitiesOK = createAction(GET_CITIES_OK)

export const DEL_ADDRESS = 'DEL_ADDRESS'
export const SET_DEFAULT_ADDRESS = 'SET_DEFAULT_ADDRESS'
export const UPDATE_ADDRESS = 'UPDATE_ADDRESS'

export const delAddress = createAction(DEL_ADDRESS)
export const setDefaultAddress = createAction(SET_DEFAULT_ADDRESS)
export const updateAddress = createAction(UPDATE_ADDRESS)

// withdraw
export const GET_MOUNT = 'GET_MOUNT'
export const SUBMIT_WITHDRAW = 'SUBMIT_WITHDRAW'
export const getMount = createAction(GET_MOUNT)
export const submitWithdraw = createAction(SUBMIT_WITHDRAW)

// getRegPoint
export const GET_REG_POINT = 'GET_REG_POINT'
export const getRegPoint = createAction(GET_REG_POINT)

// getNotice
export const GET_NOTICE = 'GET_NOTICE'
export const getNotice = createAction(GET_NOTICE)

import { takeEvery } from 'redux-saga';
import { call, put, fork, take, select } from 'redux-saga/effects';
import { hashHistory } from 'react-router';
import { Toast, Modal } from 'antd-mobile';
import { getType, changeUser } from 'Utils/auth'

import Api from '../../api'

import {
  SET_USER_TYPE,
  REQUEST_GET_USER, SUCCESS_GET_USER,
  REQUEST_CREATE_USER, SUCCESS_CREATE_USER, FAILURE_CREATE_USER,
  GET_ORDERS, GET_ORDERS_OK,
  UPDATE_USER, UPDATE_USER_OK,
  GET_INVITATION_CODE, GET_INVITATION_CODE_OK,
  GET_POINT_FOR_USER, GET_POINT_FOR_USER_OK,
  GET_POINTS, GET_POINTS_OK,
  CONFIRM_ORDER, CONFIRM_ORDER_OK,
  GET_PRODUCTS, GET_PRODUCTS_OK,
  GET_PRODUCT_DETAIL, GET_PRODUCT_DETAIL_OK,
  GET_ADDRESS_LIST, GET_ADDRESS_LIST_OK,
  CREATE_ADDRESS, CREATE_ADDRESS_OK,
  GET_CITIES, GET_CITIES_OK,
  SEND_VCODE,
  UPDATE_PHONE,
  DEL_ADDRESS,
  SET_DEFAULT_ADDRESS,
  UPDATE_ADDRESS,
  GET_MOUNT,
  SUBMIT_WITHDRAW,
  GET_SPECIAL_PRODUCT,
  INIT_USER_INFO,
  GET_IMGS,
  UPLOAD_IMGS,
  GET_REG_POINT,
  GET_NOTICE,
  requestCreateUser, successCreateUser, failureCreateUser,
} from '../actions';


// 获取注册要送的积分数
function* getRegPoint({ payload }){
   try{
     const res = yield call(Api.getRegPoint, payload);
     if(res.errorCode === 200){
        yield put({
          type: `${GET_REG_POINT}_SUCCESS`,
          payload: res
        })
      }
   }catch(e){
   }
 }

// 获取用户信息
function* getImgs({ payload }){
   try{
     const res = yield call(Api.getImgs, payload);
     if(res.errorCode === 200){
        yield put({
          type: `${GET_IMGS}_SUCCESS`,
          payload: res
        })
      }
   }catch(e){
   }
 }

function* uploadImgs({ payload }){
  try{
    const res = yield call(Api.uploadImgs, payload);
    if(res.errorCode === 200){
       yield put({
         type: `${UPLOAD_IMGS}_SUCCESS`,
         payload: payload
       })
       Toast.info('资料上传成功');
     }
  }catch(e){}
 }

// 用来更新用户信息
function* getUser({ payload }){
   try{
     const res = yield call(Api.getUser, payload);
     if(res.errorCode === 200){
       changeUser(res.data);//更新缓存中的用户信息
        yield put({
          type: SUCCESS_GET_USER,
          payload: res
        })
      }else if(res.errorCode === 401){
         yield fork(initUserInfo, {
           payload: {userInfo:{}}
         })
       }
   }catch(e){}
 }

function* initUserInfo({ payload }){
  yield put({
    type: `${INIT_USER_INFO}_SUCCESS`,
    payload: payload
  })
 }

// 注册
function* createUser({ payload }){
  try{
    const result = yield call(Api.createUser, payload);
    if(result.errorCode === 200){
      // 注册成功之后，重新获取一次用户信息
      yield put({type:REQUEST_GET_USER, payload:{type: payload.type}})
      // 已经注册，跳转到首页
      Modal.alert('系统提示', result.moreInfo, [{text:'前往首页',onPress:()=>{
        hashHistory.replace({ pathname:'/', query: {type: payload.type}});
      }}])
    }
  }catch(err){
  }
}

// 修改用户信息
function* editUser({ payload }){
  try{
    const res = yield call(Api.editUser, payload);

    if(res.errorCode === 200){
      Modal.alert('系统提示', res.moreInfo, [{text:'前往首页',onPress:()=>{
        hashHistory.push({ pathname:'/', search: `?type=1` });
      }}])
    }
    yield fork(getUser, {payload:{type: getType()}});
  }catch(e){
  }
}

function* reflashUserInfo(){
  try{
    const res = yield call(Api.reflashUserInfo);

    if(res.errorCode === 200){
      yield put({
        type: SUCCESS_GET_USER,
        payload: res
      })
    }
  }catch(e){}
}

// 获取订单
function* getOrders({ payload }){
  try{
    const res = yield call(Api.getOrders, payload);

    if(res.errorCode === 200){
      yield put({
        type:GET_ORDERS_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}

// 获取邀请码
function* getInvitationCode({ payload }){
  try{
    const res = yield call(Api.getInviCode, payload);
    if(res.errorCode === 200){
      yield put({
        type:GET_INVITATION_CODE_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}

// 获取当前用户积分
function* getPointForUser({ payload }){
  try{
    const res = yield call(Api.getPoint4User, payload);

    if(res.errorCode === 200){
      yield put({
        type:GET_POINT_FOR_USER_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}

// 获取积分列表
function* getPoints({ payload }){
  try{
    const res = yield call(Api.getPoints, payload);

    if(res.errorCode === 200){
      yield put({
        type:GET_POINTS_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}

// 确认收货
function* confirmOrder({ payload }){
  try{
    const res = yield call(Api.confirmOrder, payload);

    if(res.errorCode === 200){
      yield put({
        type: CONFIRM_ORDER_OK,
        payload,
      })
    }
  }catch(e){
  }
}

// 获取产品列表
function* getProducts({ payload }){
  try{
    const res = yield call(Api.getProducts, payload);

    if(res.errorCode === 200){
      yield put({
        type:`${GET_PRODUCTS}_OK`,
        payload: res,
      })
    }
  }catch(e){
  }
}

// 获取产品详情
function* getProductDetail({ payload }){
  try{
    const res = yield call(Api.getProductDetail, payload);

    if(res.errorCode === 200){
      yield put({
        type:GET_PRODUCT_DETAIL_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}
// 获取地址列表
function* getAddressList({ payload }){
  try{
    const res = yield call(Api.getAddressList, payload);

    if(res.errorCode === 200){
      yield put({
        type:GET_ADDRESS_LIST_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}

// 创建地址信息
function* createAddress({ payload }){
  try{
    const res = yield call(Api.createAddress, payload);

    if(res.errorCode === 200){
      yield put({
        type:CREATE_ADDRESS_OK,
        payload: res,
      })
      Toast.success('创建成功', 3, ()=>{
        hashHistory.replace({ pathname:'/pickAddress' });
      })
    }
  }catch(e){
  }
}

// 获取城市列表
function* getCities({ payload }){
  try{
    const res = yield call(Api.getCities, payload);

    if(res.errorCode === 200){
      yield put({
        type:GET_CITIES_OK,
        payload: res,
      })
    }
  }catch(e){
  }
}

function* sendVCode({ payload }){
  try{
    const res = yield call(Api.sendVCode, payload);

    if(res.errorCode === 200){
      yield put({
        type: `${SEND_VCODE}_SUCCESS`,
        payload: res,
      })
    }
  }catch(e){}
}

function* updatePhone({ payload }){
  try{
    const res = yield call(Api.updatePhone, payload)
    if(res.errorCode === 200){
      yield put({type:`${UPDATE_PHONE}_SUCCESS`, payload })
      Toast.success('修改成功', 3, ()=>{
        hashHistory.goBack();
      })
    }
  }catch(e){}
}

// 删除 - 地址信息
function* delAddress({ payload }){
  try{
    const res = yield call(Api.delAddress, payload);
    // const res = {errorCode: 200};

    if(res.errorCode === 200){
      yield put({
        type:`${DEL_ADDRESS}_SUCCESS`,
        payload,
      })
    }
  }catch(e){
  }
}
// 设置默认 - 地址信息
function* setDefaultAddress({ payload }){
  try{
    const res = yield call(Api.setDefaultAddress, payload);

    if(res.errorCode === 200){
      yield put({
        type:`${SET_DEFAULT_ADDRESS}_SUCCESS`,
        payload,
      })
      Toast.success('设置成功', 2)
    }
  }catch(e){
  }
}
// 更新 - 地址信息
function* updateAddress({ payload }){
  try{
    const res = yield call(Api.updateAddress, payload);

    if(res.errorCode === 200){
      yield put({
        type:`${UPDATE_ADDRESS}_SUCCESS`,
        payload: res,
      })
      Toast.success('更新成功', 2, ()=>{
        hashHistory.replace('/pickAddress')
      });
      yield put({type:GET_ADDRESS_LIST});
    }
  }catch(e){
  }
}

function* getMount({ payload }){
  try{
    const res = yield call(Api.getMount, payload);
    if(res.errorCode === 200){
      yield put({
        type:`${GET_MOUNT}_SUCCESS`,
        payload: res,
      })
    }
  }catch(e){}
}

function* submitWithdraw({ payload }){
  try{
    const res = yield call(Api.submitWithdraw, payload);
    if(res.errorCode === 200){
      yield put({
        type:`${SUBMIT_WITHDRAW}_SUCCESS`,
        payload: res,
      })
    }
  }catch(e){}
}

function* getSpecialProduct({ payload }){
  try{
    const res = yield call(Api.getSpecialProduct, payload)

    if(res.errorCode === 200){
      yield put({
        type: `${GET_SPECIAL_PRODUCT}_SUCCESS`,
        payload: res
      })
    }
  }catch(e){}
}

function* getNotice({ payload }){
  try{
    const res = yield call(Api.getNotice, payload);

    if(res.errorCode === 200){
      yield put({
        type: `${GET_NOTICE}_SUCCESS`,
        payload: res
      })
    }

  }catch(e){}
}

function* w_get_notice(){
  yield* takeEvery(GET_NOTICE, getNotice)
}


function* w_get_reg_point(){
  yield* takeEvery(GET_REG_POINT, getRegPoint)
}

function* w_get_imgs(){
  yield* takeEvery(GET_IMGS, getImgs)
}

function* w_upload_imgs(){
  yield* takeEvery(UPLOAD_IMGS, uploadImgs)
}

function* w_init_user_info(){
  yield* takeEvery(INIT_USER_INFO, initUserInfo)
}

function* w_get_spec_product(){
  yield* takeEvery(GET_SPECIAL_PRODUCT, getSpecialProduct)
}

function* w_del_address(){
  yield* takeEvery(DEL_ADDRESS, delAddress)
}
function* w_set_default_address(){
  yield* takeEvery(SET_DEFAULT_ADDRESS, setDefaultAddress)
}
function* w_update_address(){
  yield* takeEvery(UPDATE_ADDRESS, updateAddress)
}

function* w_update_phone(){
  yield* takeEvery(UPDATE_PHONE, updatePhone)
}

function* w_send_vcode(){
  yield* takeEvery(SEND_VCODE, sendVCode);
}

function* w_get_point_for_user(){
  yield* takeEvery(GET_POINT_FOR_USER, getPointForUser);
}

function* w_get_points(){
  yield* takeEvery(GET_POINTS, getPoints);
}

function* w_confirm_order(){
  yield* takeEvery(CONFIRM_ORDER, confirmOrder);
}

function* w_get_products(){
  yield* takeEvery(GET_PRODUCTS, getProducts);
}
function* w_get_product_detail(){
  yield* takeEvery(GET_PRODUCT_DETAIL, getProductDetail);
}

function* w_get_address_list(){
  yield* takeEvery(GET_ADDRESS_LIST, getAddressList);
}
function* w_create_address(){
  yield* takeEvery(CREATE_ADDRESS, createAddress);
}

function* w_get_cities(){
  yield* takeEvery(GET_CITIES, getCities);
}

function* w_get_invitation_code(){
  yield* takeEvery(GET_INVITATION_CODE, getInvitationCode);
}

function* w_create_user(){
  yield* takeEvery(REQUEST_CREATE_USER, createUser);
}

function* w_get_user(){
  yield* takeEvery(REQUEST_GET_USER, getUser);
}

function* w_update_user(){
  yield* takeEvery(UPDATE_USER, editUser);
}

function* w_get_orders(){
  yield* takeEvery(GET_ORDERS, getOrders);
}

function* w_get_mount(){
  yield* takeEvery(GET_MOUNT, getMount);
}

function* w_submit_withdraw(){
  yield* takeEvery(SUBMIT_WITHDRAW, submitWithdraw);
}

export default function* sagas() {
  yield fork(w_create_user)
  yield fork(w_get_user)
  yield fork(w_update_user)
  yield fork(w_get_orders)
  yield fork(w_get_point_for_user)
  yield fork(w_get_points)
  yield fork(w_confirm_order)
  yield fork(w_get_products)
  yield fork(w_get_product_detail)
  yield fork(w_get_address_list)
  yield fork(w_create_address)
  yield fork(w_get_cities)
  yield fork(w_get_invitation_code)
  yield fork(w_update_phone)
  yield fork(w_send_vcode)
  yield fork(w_del_address)
  yield fork(w_set_default_address)
  yield fork(w_update_address)
  yield fork(w_get_mount)
  yield fork(w_submit_withdraw)
  yield fork(w_init_user_info)
  yield fork(w_get_spec_product)
  yield fork(w_get_imgs)
  yield fork(w_upload_imgs)
  yield fork(w_get_reg_point)
  yield fork(w_get_notice)
}

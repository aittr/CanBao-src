import { combineReducers } from 'redux'
import Immutable from 'seamless-immutable'
import _ from 'lodash'

import {
  REQUEST_CREATE_USER,
  SUCCESS_CREATE_USER,
  FAILURE_CREATE_USER,
  SET_USER_TYPE,
  SUCCESS_GET_USER,

  GET_INVITATION_CODE_OK,
  UPDATE_USER_OK,
  GET_PRODUCTS_OK,
  GET_PRODUCT_DETAIL_OK,
  GET_ORDERS_OK,
  CONFIRM_ORDER_OK,
  GET_POINT_FOR_USER_OK,
  GET_POINTS_OK,
  GET_ADDRESS_LIST_OK,
  CREATE_ADDRESS_OK,
  UPDATE_PHONE,
  DEL_ADDRESS,
  SET_DEFAULT_ADDRESS,
  UPDATE_ADDRESS,

  GET_MOUNT,
  SUBMIT_WITHDRAW,
  GET_SPECIAL_PRODUCT,
  INIT_USER_INFO,
  GET_IMGS,
  UPLOAD_IMGS,
  GET_REG_POINT,
  GET_NOTICE,
} from './actions';

const initial = Immutable({
  app:{
    userType:1,
    userInfo:{},
    invitationCode:'',
    imgs:[],
    notice:{},
  },
  order:{},
  point:{},
  address:{},
  product:{},
});

// app
function app(state = initial.app, { type, payload }) {
  switch (type) {
    case SET_USER_TYPE:
      return Immutable.merge(state, { userType: payload.type });
    case SUCCESS_GET_USER:
      return Immutable.merge(state, { userInfo: payload.data });
    case GET_INVITATION_CODE_OK:
      return Immutable.merge(state, { invitationInfo: payload.data });
    case UPDATE_USER_OK:
      return Immutable.merge(state, { userInfo: payload.data });
    case `${UPDATE_PHONE}_SUCCESS`:
      return Immutable.merge(state, {
        userInfo: {phone: payload.phone }
      }, {deep: true});
    case `${GET_MOUNT}_SUCCESS`:
      return Immutable.merge(state, { mountList: payload.data });
    case `${INIT_USER_INFO}_SUCCESS`:
      return Immutable.merge(state, { userInfo: payload.userInfo });
    case `${GET_SPECIAL_PRODUCT}_SUCCESS`:
      return Immutable.merge(state, { specialProduct: payload.data });
    case `${GET_IMGS}_SUCCESS`:
      return Immutable.merge(state, { imgs: payload.data });
    case `${UPLOAD_IMGS}_SUCCESS`:
      return Immutable.merge(state, { imgs: payload.imgs });
    case `${GET_NOTICE}_SUCCESS`:
      return Immutable.merge(state, { notice: payload.data });
  }
  return state;
}

// product
function product(state = initial.product, { type, payload }){
  switch (type) {
    case GET_PRODUCTS_OK:
      return Immutable.merge(state, { list: payload.data });
    case GET_PRODUCT_DETAIL_OK:
      return Immutable.merge(state, { proNumbers: payload.data })
    default:
      return state;
  }
}

// order
function order(state = initial.order, { type, payload }) {
  switch (type) {
    case GET_ORDERS_OK:
      return Immutable.merge(state, {list: payload.data});
    case CONFIRM_ORDER_OK:
      let list = state.list;
      if(list && list.lenght){
        list.map(v=>{
          if(v.id === payload.distId){
            v.status = 4;
          }
          return v;
        })
      }
      return Immutable.merge(state,{list: list});
      // 修改当前详细的状态
      return state;
    default:
      return state;
  }
}

// point
function point(state = initial.point, { type, payload }){
  switch (type) {
    case GET_POINT_FOR_USER_OK:
      return Immutable.merge(state, {currPoint: payload.data});
    case GET_POINTS_OK:
      return Immutable.merge(state, {list: payload.data})
    case `${GET_REG_POINT}_SUCCESS`:
      return Immutable.merge(state, {regPoint: payload.data})
    default:
      return state;
  }
}

// address
function address(state = initial.address, { type, payload }){
  switch (type) {
    case GET_ADDRESS_LIST_OK:
      return Immutable.merge(state, { list: payload.data}, {deep: true});
    case CREATE_ADDRESS_OK:
      return Immutable.merge(state, { detail: payload.data}, {deep: true})
    case `${DEL_ADDRESS}_SUCCESS`:
      const remainList = state.list.filter(v=>v.id !== payload.customerAddressId)
      return Immutable.set(state, 'list', remainList);
    case `${SET_DEFAULT_ADDRESS}_SUCCESS`:
      const newlist = state.list.map(v=>{
        if(v.id === payload.customerAddressId){
          v = v.set('default', true);
        }else{
          v = v.set('default', false);
        }
        return v;
      })

      return Immutable.set(state, 'list', newlist);
    default:
      return state;
  }
}


export default combineReducers(
  { app, address, point, order, product }
);
